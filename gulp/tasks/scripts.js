var gulp = require('gulp'),
    webpack = require('webpack');

gulp.task('scripts', function(callback){
    webpack(require('../../webpack.config.js'), function(err, stats){
        if(err){
            console.log(err.toString());
        }
        console.log(stats.toString());
        //return console.log("Completed Webpack Activity! Your JS have been bundled!");
        //As in js functions are ASynchronus..Either we have to return the statements or give the name to the function and call the function in the body ..As JS says that by calling function in it's own body will end the THREAD!
        callback();
    });
});