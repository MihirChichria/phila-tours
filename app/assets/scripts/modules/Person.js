//function Person(fullName, favColor){
//    this.name = fullName;
//    this.favColor = favColor;
//    
//    this.greet = function(){
//        console.log("Hey Welcome! My name is " + this.name + " and I love " + this.favColor + " color!");
//    }
//}
class Person{
    constructor(fullName, favColor){
        this.name = fullName;
        this.favColor = favColor;
    }
    greet(){
        console.log("Hey Welcome! My name is " + this.name + " and I love " + this.favColor + " color!");
    }
}

//module.exports = Person;

export default Person;
//export Mihir Person;->Abhi jaha pe import krenge waha pe import Mihir from .. likhna padega 